/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ENEMY_H
#define ENEMY_H

#include "character.h"

class Enemy : public Character
{
    Q_OBJECT

public:
    Enemy(QObject *parent, QString characterName);
    ~Enemy();


signals:


public slots:
    void move();

    void draw(QPainter *painter, int offsetX, int offsetY);

private:
    QPixmap spritePixmap1;

    QString name;


};



#endif // ENEMY_H
