/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "player.h"


Player::Player(QObject *parent) :  Character(parent)
{
    this->screenX = 120;
    this->screenY = 450;


    // Initialize player status
    life = 400;
    power = 0;

    lookingLeft = false;

    // mirror pixmaps like this:
    // QPixmap::fromImage(QImage(":/img/player/player1.png").mirrored(true,false));


    // Load pixmaps for the sprite
    QList<QPixmap> standingPixmaps;
    QList<QPixmap> walkingPixmaps;
    QList<QPixmap> jumpingPixmaps;
    QList<QPixmap> attackPixmaps;


    bool useMirror = false;

    // Loop TWO times, one without mirroring pixmaps, the other mirroring them
    for (int counter = 0; counter !=2; ++counter)
    {
        qDebug() << "Player::Player() adding Pixmaps, mirror" << useMirror;
        standingPixmaps << QPixmap::fromImage(QImage(":/img/player/standing0.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/standing1.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/standing2.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/standing3.png")
                                              .mirrored(useMirror, false));

        walkingPixmaps  << QPixmap::fromImage(QImage(":/img/player/walking0.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/walking1.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/walking2.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/walking3.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/walking4.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/walking5.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/walking6.png")
                                              .mirrored(useMirror, false))
                        << QPixmap::fromImage(QImage(":/img/player/walking7.png")
                                              .mirrored(useMirror, false));

        jumpingPixmaps  << QPixmap::fromImage(QImage(":/img/player/jumping0.png")
                                              .mirrored(useMirror, false));

        attackPixmaps   << QPixmap::fromImage(QImage(":/img/player/attack0.png")
                                              .mirrored(useMirror, false));

        useMirror = true; // for second loop
    }


    spritePixmaps << standingPixmaps
                  << walkingPixmaps
                  << jumpingPixmaps
                  << attackPixmaps;






    this->width = walkingPixmaps.at(0).width();
    this->height = walkingPixmaps.at(0).height();

    shadeOffsetX = (width/2) - (shadeWidth/2);
    shadeOffsetY = height - (shadeHeight/2) - 8;



    qDebug() << "Player created";
}


Player::~Player()
{
    qDebug() << "Player destroyed";
}





//
// Slots
//



/*
 *  Keyboard control: keys PRESSED
 */
void Player::processKeyPresses(QKeyEvent *event)
{
    switch (event->key())
    {

    case Qt::Key_Up:
        up = true;
        down = false;
        break;

    case Qt::Key_Down:
        down = true;
        up = false;
        break;

    case Qt::Key_Left:
        left = true;
        right = false;
        break;

    case Qt::Key_Right:
        right = true;
        left = false;
        break;


    case Qt::Key_Insert:
        attacking = true;
        if (this->life > 1)
        {
            life-=2; // TMP
        }
        if (this->power < 200)
        {
            this->power++;
        }
        emit playSound("punch");
        break;

    case Qt::Key_Delete:
        if (!jumpingUp && !jumpingDown)
        {
            jumpingUp = true;
            jumpPhase = 0;
        }
        this->power = 0;
        break;

    }
}



/*
 *  Keyboard control: keys RELEASED
 */
void Player::processKeyReleases(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Up:
        up = false;
        break;

    case Qt::Key_Down:
        down = false;
        break;

    case Qt::Key_Left:
        left = false;
        break;

    case Qt::Key_Right:
        right = false;
        break;

        //tmp
    case Qt::Key_Insert:
        attacking = false;
        break;


    }
}



void Player::move()
{
    currentAction = aStanding; // Stand by default. Will be changed below if applicable


    if (up)
    {
        currentAction = aWalking;

        if (screenY > 50)
        {
            screenY -= 2;
            ++frameCounter;
            if (frameCounter >= 6)
            {
                frameCounter = 0;
                --currentFrame[aWalking];
                if (currentFrame.at(aWalking) == -1)
                {
                    currentFrame[aWalking] = 7;
                }
            }
        }
    }
    if (down)
    {
        currentAction = aWalking;

        if (screenY < this->stageHeight-100) // tmp
        {
            screenY += 2;
            ++frameCounter;
            if (frameCounter >= 6)
            {
                frameCounter = 0;
                ++currentFrame[aWalking];
                if (currentFrame.at(aWalking) == 8)
                {
                    currentFrame[aWalking] = 0;
                }
            }
        }
    }
    if (left)
    {
        currentAction = aWalking;
        lookingLeft = true;

        if (screenX > -50)
        {
            screenX -= 2;
            if (!up && !down)
            {
                ++frameCounter;
            }
            if (frameCounter >= 6)
            {
                frameCounter = 0;
                --currentFrame[aWalking];
                if (currentFrame.at(aWalking) == -1)
                {
                    currentFrame[aWalking] = 7;
                }
            }
        }
    }
    if (right)
    {
        currentAction = aWalking;
        lookingLeft = false;

        if (screenX < this->stageWidth - 100) // tmp
        {
            screenX += 2;
            if (!up && !down)
            {
                ++frameCounter;
            }
            if (frameCounter >= 6)
            {
                frameCounter = 0;
                ++currentFrame[aWalking];
                if (currentFrame.at(aWalking) == 8)
                {
                    currentFrame[aWalking] = 0;
                }
            }

        }
    }


    if (currentAction == aStanding) // if still standing, not moving
    {
        currentFrame[aWalking] = 0; // reset walking animation

        ++frameCounter;
        if (frameCounter >= 8)
        {
            frameCounter = 0;
            ++currentFrame[aStanding];
            if (currentFrame.at(aStanding) == 4)
            {
                currentFrame[aStanding] = 0;
            }
        }

    }


    // Jump control
    if (jumpingUp)
    {
        currentAction = aJumping;
        currentFrame[aJumping] = 0;

        jumpPhase += 6;
        if (jumpPhase >= 140)
        {
            jumpingUp = false;
            jumpingDown = true;
        }
    }
    else if (jumpingDown)
    {
        currentAction = aJumping;
        currentFrame[aJumping] = 0;

        jumpPhase -= 6;
        if (jumpPhase <= 0) // end of jump
        {
            jumpingDown = false;
            jumpPhase = 0;
            emit playSound("hitfloor");
        }
    }


    if (attacking)
    {
        currentAction = aAttacking;
        currentFrame[aAttacking] = 0;
    }



    ///////////////////////////////// very TMP
    if (currentAction == aStanding)
    {
        if (this->life < 400)
        {
            ++life;
        }
    }


}


/*
 *  Draw Player sprite, including the shade
 */
void Player::draw(QPainter *painter)
{
    // draw player's shade pixmap
    painter->drawPixmap(this->screenX + shadeOffsetX,
                        this->screenY + shadeOffsetY,
                        this->shadePixmap);


    // TMP calculation for correct mirrored frame
    int frameOffset = 0;
    if (lookingLeft)
    {
        frameOffset = spritePixmaps.at(currentAction).length()/2;
    }


    // draw player pixmap
    painter->drawPixmap(this->screenX,
                        this->screenY - jumpPhase,
                        spritePixmaps.at(currentAction)  // at actionType . at currentFrame
                                     .at(currentFrame.at(currentAction) + frameOffset));

    // debug info for animation frames
    //painter->drawText(screenX, screenY, QString("%1").arg(frameCounter));
}
