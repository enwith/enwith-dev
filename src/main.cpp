/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */


#include <QApplication>
#include <iostream>
#include "mainwindow.h"


int main(int argc, char *argv[])
{
    QApplication ewApp(argc, argv);
    ewApp.setApplicationName("Enwith");
    ewApp.setApplicationVersion("0.09");
    ewApp.setOrganizationName("JanCoding");
    ewApp.setOrganizationDomain("jancoding.wordpress.com");

    std::cout << QString("Enwith v%1 - JanKusanagi 2011-2015\n\n")
                 .arg(ewApp.applicationVersion()).toStdString();
    std::cout.flush();


    MainWindow ewWindow;
    ewWindow.show();
    //ewWindow.showMaximized();
    //ewWindow.showFullScreen();

    return ewApp.exec();
}
