/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "character.h"


Character::Character(QObject *parent) :  QObject(parent)
{
    up = false;
    down = false;
    left = false;
    right = false;

    attacking = false;
    jumpingUp = false;
    jumpingDown = false;
    jumpPhase = 0;

    //
    // Load pixmaps here FIXME
    //


    shadePixmap = QPixmap(":/img/shade.png");

    shadeWidth = shadePixmap.width();
    shadeHeight = shadePixmap.height();



    this->currentFrame << 0  // standing
                       << 0  // walking
                       << 0  // jumping
                       << 0; // attacking

    currentAction = aStanding;

    this->frameCounter = 0;


    qDebug() << "Character created";
}


Character::~Character()
{
    qDebug() << "Character destroyed";
}



int Character::getX()
{
    //qDebug() << "X" << this->x;
    return this->screenX;
}

int Character::getY()
{
    //qDebug() << "Y" << this->y;
    return this->screenY;
}


int Character::getZ()
{
    z = this->screenY + this->height;
    //qDebug() << "Z" << this->z;
    return this->z;
}


unsigned int Character::getLife()
{
    return this->life;
}

unsigned int Character::getPower()
{
    return this->power;
}


//
// Slots
//

/*
 * Update internal knowledge about stage size
 */
void Character::setStageSize(int newWidth, int newHeight)
{
    this->stageWidth = newWidth;
    this->stageHeight = newHeight;

    qDebug() << "Character::setStageSize()" << stageWidth << stageHeight;
}
