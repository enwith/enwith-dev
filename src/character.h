/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CHARACTER_H
#define CHARACTER_H

#include <QObject>
#include <QKeyEvent>
#include <QPainter>
#include <QPixmap>
#include <QImage>

#include <QList>

#include <QDebug>


class Character : public QObject
{
    Q_OBJECT

public:
    Character(QObject *parent = 0);
    ~Character();

    int getX();
    int getY();
    int getZ();

    unsigned int getLife();
    unsigned int getPower();


signals:
    void playSound(QString sfxType);

public slots:
    void setStageSize(int newWidth, int newHeight);


protected:
    QPixmap shadePixmap;
    QList< QList <QPixmap> > spritePixmaps;
    /* Use QImages to flip horizontally the sprites */
    int frameCounter;

    enum Actions { aStanding,
                   aWalking,
                   aJumping,
                   aAttacking
                 };
    int currentAction;
    QList<int> currentFrame;

    int width;
    int height;
    int screenX;
    int screenY;

    int shadeWidth;
    int shadeHeight;
    int shadeOffsetX;
    int shadeOffsetY;

    int x;
    int y;
    int z;

    bool lookingLeft;

    bool up;
    bool down;
    bool left;
    bool right;

    bool attacking;
    bool jumpingUp;
    bool jumpingDown;
    int jumpPhase;

    unsigned int life;
    unsigned int power;


    int stageWidth;
    int stageHeight;


};

#endif // CHARACTER_H
