/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "stage.h"

Stage::Stage(QObject *parent)  :  QObject(parent)
{
    backgroundFilenames.clear();
    backgroundFilenames.append(":/img/stage/forest2.png");
    backgroundFilenames.append(":/img/stage/beach.png");
    backgroundFilenames.append(":/img/stage/forest3.png");
    backgroundFilenames.append(":/img/stage/city.png");


    foregroundFilenames.clear();
    foregroundFilenames.append(":/img/stage/fog3.png");
    foregroundFilenames.append("");
    foregroundFilenames.append(":/img/stage/rain.png");
    foregroundFilenames.append("");



    this->currentStage = 0; // Start on first level

    backgroundX = 0;
    backgroundY = 0;
    this->backgroundPixmap = QPixmap(backgroundFilenames.at(currentStage));

    foregroundX = 0;
    foregroundY = 0;
    this->foregroundPixmap = QPixmap(foregroundFilenames.at(currentStage));


    this->width = 100;  // Initial values; will be overridden on mainWindow::resizeEvent()
    this->height = 100;


    qDebug() << "Stage created";
}


Stage::~Stage()
{
    qDebug() << "Stage destroyed";
}


int Stage::getOffsetX()
{
    return this->backgroundX;
}

int Stage::getOffsetY()
{
    return this->backgroundY;
}




/*
 *  Load a new stage: load images, define foreground, etc.
 */
void Stage::load(int numStage)
{
    currentStage = numStage;

    //this->maxZ =

    // TMP, for now, just this
    backgroundPixmap = QPixmap(backgroundFilenames.at(currentStage));
    foregroundPixmap = QPixmap(foregroundFilenames.at(currentStage));
}


/*
 *  Move the background position according to the player's position
 *  Move the foreground according to what it is (like scrolling fog, or falling rain)
 */
void Stage::move(int playerX, int playerY)
{
    backgroundX = playerX * -1; // TMP calculation... or is it? :D
    if (backgroundX > 0)
    {
        backgroundX = 0;
    }
    else if (backgroundX < (this->width * -1))
    {
        backgroundX = this->width * -1;
    }

    Q_UNUSED(playerY); // for now


    --foregroundX;
    if (foregroundX < this->width * -1) // TMP
    {
        foregroundX = 0;
    }
}


/*
 *  Update Stage's internal knowledge of the MainWindow's size
 */
void Stage::setSize(int windowWidth, int windowHeight)
{
    this->width = windowWidth;
    this->height = windowHeight;

    //emit sizeChanged(width, height);
}


/*
 *  Draw the background of the stage, including possible scrolls (parallax)
 */
void Stage::drawBackground(QPainter *painter)
{
    painter->drawPixmap(backgroundX, backgroundY,
                        this->width * 2, this->height,
                        backgroundPixmap);
}


/*
 *  Draw the foreground of the stage: fog, rain, snow...
 */
void Stage::drawForeground(QPainter *painter)
{

    painter->drawPixmap(foregroundX, foregroundY,
                        this->width, this->height,
                        foregroundPixmap);

    painter->drawPixmap(foregroundX + this->width, foregroundY,
                        this->width, this->height,
                        foregroundPixmap);


}
