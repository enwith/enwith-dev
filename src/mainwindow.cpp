/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "mainwindow.h"

/*
 *  Constructor
 */
MainWindow::MainWindow(QWidget *parent)  : QGLWidget(parent)
{
    this->setWindowTitle("Endless/Withstand");

    // hide mouse cursor
    this->setCursor(QCursor(Qt::BlankCursor));  // TMP?

    // Should help performance
    this->setAutoFillBackground(false);

    // This helps avoid fullscreen flickering; requires swapBuffers() at end of paintGL()
    // Also, improves performance greatly. Qt bug?
    this->setAutoBufferSwap(false);

    this->gameFont = QFont();
    gameFont.setBold(true);
    gameFont.setPointSize(16);
    gameFont.setFamily("Courier");
    //gameFont.setStyleHint(QFont::TypeWriter);


    this->textX = 600;
    this->textY = 500;


    // load saved configuration from file
    loadSettings();

    // randomize pool depending on current time
    qsrand(QTime::currentTime().second());

    this->elapsedSec = 0;
    this->fps = 0;
    this->fpsCounter = 0;

    playing = false;
    this->currentStage = 0;

    this->playerName = qgetenv("USER").constData();


    stage = new Stage(this);
    player = new Player(this);
    connect(player, SIGNAL(playSound(QString)),
            this,   SLOT(soundPlay(QString)));


    enemiesNames << "Joe" << "Smith" << "Pancracio" << "Rigoberto"
                 << "Fulgencio" << "Rogelio" << "Jarl!" << "Pepo"
                 << "Wenceslao" << "Gumersindo" << "Eleuterio" << "Fervoroso"
                 << "Romualdo" << "Telesforo";

    for (int counter=0; counter < enemiesNames.size(); ++counter)
    {
         if (counter == 0)
         {
             break; // tmp limit
         }
         enemies.append(new Enemy(this, enemiesNames.at(counter)));
    }



    /*
     * Sound init
     */
    /*QAudioFormat audioFormat;
    audioFormat.setFrequency(44100);   // 44 KHz
    audioFormat.setSampleSize(16);     // 16 bits per sample
    audioFormat.setChannelCount(1);    // Mono
    audioFormat.setCodec("audio/pcm"); // regular PCM
    audioFormat.setSampleType(QAudioFormat::SignedInt); // Signed


    audioOutput = new QAudioOutput(audioFormat, this);

    // Load sound samples
    sfxPunch.setFileName(":/snd/punch.pcm");
    sfxPunch.open(QIODevice::ReadOnly);

    sfxHitFloor.setFileName(":/snd/hitfloor.pcm");
    sfxHitFloor.open(QIODevice::ReadOnly);
    */


    // One-shot timer to call post-init stuff
    this->postInitTimer = new QTimer(this);
    postInitTimer->setSingleShot(true);
    connect(postInitTimer, SIGNAL(timeout()), SLOT(postInit()));
    postInitTimer->start(10);



    // Timer with 20 msec interval:  1000 msec in a sec / 20 msec = 50 fps
    this->timer = new QTimer(this);
    timer->setInterval(20); // 50 fps
    connect(timer, SIGNAL(timeout()), SLOT(timerTick()));
    timer->start();

    // Another timer to calculate FPS. Interval 1 sec.
    this->fpsTimer = new QTimer(this);
    fpsTimer->setInterval(1000);
    connect(fpsTimer, SIGNAL(timeout()), SLOT(fpsTick()));
    fpsTimer->start();

    qDebug() << "Enwith MainWindow created...";
}


/*
 *  Destructor
 */
MainWindow::~MainWindow()
{
    qDebug() << "Destroying Enwith MainWindow...";
}



/*
 *  Paint Event
 */
void MainWindow::paintGL()
{
    QPainter gamePainter(this);
    gamePainter.setRenderHints(QPainter::SmoothPixmapTransform, this->smooth);

    gamePainter.setFont(this->gameFont);


    if (!playing) // Title screen
    {
        gamePainter.drawPixmap(0, 0,
                           this->width(), this->height(),
                           QPixmap(":/img/titlescreen-v0.png"));


        gamePainter.setPen(QColor("red"));
        gamePainter.drawText(this->textX, this->textY, tr("Press Enter to play, or F9 for options"));
    }
    else
    {
        stage->drawBackground(&gamePainter);


        // A QMap<int, int> key:value list
        // It will store Zpos:numEnemy (Zpos:-1 for the player)
        zSortList.clear();

        // Insert all enemies in the list
        int numEnemies = enemies.size();
        for (int counter=0; counter != numEnemies; ++counter)
        {
            // Zpos: numEnemy   Key:value pair
            zSortList.insertMulti(enemies.at(counter)->getZ(), counter);
            // insertMULTI, to allow repeated keys (since Zpos is the key)
        }
        // Insert the player position in the list
        zSortList.insertMulti(player->getZ(), -1);


        foreach (int zValue, zSortList) // get each value from the key:value pair
        {
            if (zValue != -1) // If it's NOT -1, it's an enemy
            {
                enemies.at(zValue)->draw(&gamePainter,
                                         stage->getOffsetX(),
                                         stage->getOffsetY());
            }
            else // if IT IS -1, it's the player
            {
                player->draw(&gamePainter);
            }
        }

        stage->drawForeground(&gamePainter);

        drawGameInfo(&gamePainter);
    }


    gamePainter.end();

    this->swapBuffers(); // required because of setAutoBufferSwap(false);

    ////////////////////////// end of paintGL()
}


void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (playing)
    {
        this->player->processKeyPresses(event);
    }

    switch (event->key())
    {
    case Qt::Key_Return:
        this->playing = !this->playing;
        this->textX = this->width();
        break;

    case Qt::Key_Escape:
        //this->audioOutput->stop();
        this->close();
        break;


/*
    case Qt::Key_Insert: // TMP, used by player to hit; used here to play sound
        this->sfxPunch.seek(0);
        this->audioOutput->start(&sfxPunch);
        break;

    case Qt::Key_Delete: // TMP, used by player to jump; used here to play sound
        this->sfxHitFloor.seek(0);
        this->audioOutput->start(&sfxHitFloor);
        break;
*/

////// TEMP keys for tests

    case Qt::Key_F1:
        if (currentStage > 0)
        {
            --currentStage;
        }
        stage->load(currentStage);
        break;
    case Qt::Key_F2:
        if (currentStage < 3)
        {
            ++currentStage;
        }
        stage->load(currentStage);
        break;

    case Qt::Key_F3:
        this->frameSkip = !frameSkip;
        break;
    case Qt::Key_F4:
        this->smooth = !smooth;
        break;


    case Qt::Key_Minus:
        if (enemies.size() > 0)
        {
            delete enemies.last();
            enemies.removeLast();
        }
        break;
    case Qt::Key_Plus:
        enemies.append(new Enemy(this, enemiesNames.at(qrand() % enemiesNames.size())));
        // make this enemy aware of stage size
        enemies.last()->setStageSize(this->width(), this->height());
        break;


    case Qt::Key_F11:
        windowedMode = !windowedMode;
        if (windowedMode)
        {
            qDebug() << "F11: setting windowed mode";
            this->setWindowFlags(Qt::Window); // recover window frame
            this->resize(800, 600);
            this->showNormal();
        }
        else
        {
            qDebug() << "F11: setting fullscreen mode";
            //this->setWindowFlags(Qt::FramelessWindowHint); // Remove windeco
            //this->resize(320, 200);//test
            //this->showMaximized();
            this->showFullScreen();
        }
        break;


    case Qt::Key_F9: // simulation of options dialog
        if (!playing)
        {
            QMessageBox::about(this, "Configuration",
                               "Key configuration:   \n\nUp:\nDown:\nLeft:\nRight:\nAttack:\nJump:\n\n\n");
        }
        break;



    }

    event->accept();
}



void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if (playing)
    {
        player->processKeyReleases(event);
    }

    event->accept();
}



void MainWindow::resizeEvent(QResizeEvent *event)
{
    stage->setSize(this->width(), this->height());
    player->setStageSize(this->width(), this->height());
    for (int counter = 0; counter != enemies.size(); ++counter)
    {
        enemies.at(counter)->setStageSize(this->width() * 2, this->height());
    }

    event->accept();
}



/*
 *  Save settings upon closing the game window
 */
void MainWindow::closeEvent(QCloseEvent *event)
{
    this->saveSettings();

    event->accept();
}



void MainWindow::loadSettings()
{
    QSettings settings;

    // frameskip disabled by default
    this->frameSkip = settings.value("frameSkip", false).toBool();

    // pixmap smoothing enabled by default
    this->smooth = settings.value("smooth", true).toBool();

    // default is fullscreen mode
    this->windowedMode = settings.value("windowedMode", false).toBool();

    qDebug() << "Settings loaded";
}


void MainWindow::saveSettings()
{
    QSettings settings;

    settings.setValue("frameSkip", this->frameSkip);
    settings.setValue("smooth", this->smooth);
    settings.setValue("windowedMode", this->windowedMode);

    qDebug() << "Settings saved";
}



//
// Slots
//

void MainWindow::postInit()
{
    qDebug() << "postInit()";
    if (windowedMode)
    {
        this->setWindowFlags(Qt::Window);
        this->showNormal();
        this->resize(800, 600);
    }
    else  // fullscreen
    {
        //this->setWindowFlags(Qt::FramelessWindowHint);
        //this->showMaximized();
        this->showFullScreen();
    }


    this->playing = true; // TMP to skip title screen for now
}


void MainWindow::timerTick()
{
    ++this->elapsedSec;

    if (playing)
    {
        player->move();

        int numEnemies = enemies.size();
        for (int counter=0; counter != numEnemies; ++counter)
        {
            enemies.at(counter)->move();
        }

        stage->move(player->getX(), player->getY());
    }
    else
    {
        // Main menu screen
        --this->textX;
    }

    ++fpsCounter;

    // Paint 1 out of every 2 frames if frameSkip is on
    if (!frameSkip || (frameSkip && (this->elapsedSec & 1)))
    {
        this->updateGL();
    }

}


// Every second
void MainWindow::fpsTick()
{
    this->fps = this->fpsCounter;
    this->fpsCounter = 0;
    //qDebug() << "FPStick" << this->fps;
}




/*
 *  Draw indicators: life bar, scores, time...
 */
void MainWindow::drawGameInfo(QPainter *painter)
{
    painter->setPen(QColor("black"));
    //painter->setBrush(Qt::CrossPattern);

    // Life bar, background
    painter->setOpacity(0.3);
    painter->setBrush(QBrush(QColor("darkRed"), Qt::SolidPattern));
    painter->drawRect(10, 10,
                      400, 20);

    // Life bar, fill
    painter->setOpacity(1.0);
    painter->setBrush(QBrush(QColor("yellow"), Qt::SolidPattern));
    painter->drawRect(10, 10,
                      player->getLife(), 20);

    // Power bar, background
    painter->setOpacity(0.3);
    painter->setBrush(QBrush(QColor("darkRed"), Qt::SolidPattern));
    painter->drawRect(10, 35,
                      200, 10);

    // Power bar, fill
    painter->setOpacity(1.0);
    painter->setBrush(QBrush(QColor("cyan"), Qt::SolidPattern));
    painter->drawRect(10, 35,
                      player->getPower(), 10);



    painter->setPen(QColor("orange"));
    painter->drawText(10, 75,
                      QString("Player: %1 | Stage: %2 | Enemies: %3")
                      .arg(this->playerName)
                      .arg(currentStage + 1)
                      .arg(enemies.size()));

    painter->setPen(QColor("lightGreen"));
    painter->drawText(10, 100,
                      QString("X: %1 | Y: %2")
                      .arg(player->getX())
                      .arg(player->getY()));


    painter->setPen(QColor("red"));
    painter->drawText(this->width()/2, 30,
                      QString("FPS: %1 | Frameskip: %2 | Smooth: %3")
                      .arg(this->fps)
                      .arg(frameSkip ? "ON" : "OFF")
                      .arg(smooth ? "ON" : "OFF"));




    painter->setPen(QColor("lightGray"));
    painter->drawText(10, this->height()-10,
                      "F1/F2: Stage | +/-: Enemies | F3: frameskip | F4: smooth | F11: FS/WM");
}



void MainWindow::soundPlay(QString sfxType)
{
    // quite TMP
    if (sfxType == "punch")
    {
        //this->sfxPunch.seek(0);
        //this->audioOutput->start(&sfxPunch);
    }
    else
    {
        if (sfxType == "hitfloor")
        {
            //this->sfxHitFloor.seek(0);
            //this->audioOutput->start(&sfxHitFloor);
        }
    }
}
