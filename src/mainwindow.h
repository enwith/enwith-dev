/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGLWidget>
#include <QPainter>
#include <QFont>

#include <QKeyEvent>
#include <QTimer>
#include <QSettings>

#include <QTime>

#include <QList>
#include <QMap>

// Sound support
#include <QFile>
//#include <QAudioOutput>
//#include <QAudioFormat>
//#include <QAudioDeviceInfo>


#include <QDebug>

// for pow()
//#include <math.h>


#include <QMessageBox>

#include "stage.h"
#include "player.h"
#include "enemy.h"


class MainWindow : public QGLWidget
{
    Q_OBJECT


public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void paintGL();
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void resizeEvent(QResizeEvent *event);
    void closeEvent(QCloseEvent *event);

    void loadSettings();
    void saveSettings();


public slots:
    void postInit();
    void timerTick();
    void fpsTick();

    void drawGameInfo(QPainter *painter);

    void soundPlay(QString sfxType);

private:
    QFont gameFont;
    QTimer *postInitTimer;
    QTimer *timer;
    QTimer *fpsTimer;

    bool windowedMode;

    int textX, textY;

    int elapsedSec;
    int fps;
    int fpsCounter;

    bool frameSkip;
    bool smooth;

    bool playing;
    int currentStage;

    QString playerName;

    Stage *stage;
    Player *player;
    QList<Enemy*> enemies;
    QStringList enemiesNames;

    /* A key:value list; It will store Zpos:numEnemy
     * The Zpos of the player will be stored, with -1 instead of numEnemy */
    QMap<int, int> zSortList;


    //QAudioOutput *audioOutput;
    QFile sfxPunch;
    QFile sfxHitFloor;
};



#endif // MAINWINDOW_H
