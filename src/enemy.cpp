/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "enemy.h"

Enemy::Enemy(QObject *parent, QString characterName) :  Character(parent)
{
    this->name = characterName;

    this->screenX = -50 + (qrand() % 1700);
    this->screenY = 100 + (qrand() % 600);

    // Initialize enemy status
    life = 100;
    power = 0;


    // Load pixmaps for the sprite
    this->spritePixmap1 = QPixmap(QString(":/img/enemy/enemy%1.png")
                                  .arg((qrand() % 6) + 1));


    this->width = spritePixmap1.width();
    this->height = spritePixmap1.height();

    shadeOffsetX = (width/2) - (shadeWidth/2);
    shadeOffsetY = height - (shadeHeight/2) - 8;

///// TMP movement!!
    switch (qrand() % 12)
    {
    case 0:
        this->left = true;
        break;
    case 1:
        this->right = true;
        this->down = true;
        break;
    case 2:
        this->left = true;
        this->up = true;
        break;
    case 3:
        this->down = true;
        break;

    default: // no movement
        break;
    }
///

    qDebug() << "Enemy created:" << name;
}


Enemy::~Enemy()
{
    qDebug() << "Enemy destroyed:" << name;
}





void Enemy::move()
{
    if (up)
    {
        --screenY;
        if (screenY < 100)
        {
            up = false;
            down = true;
        }
    }
    if (down)
    {
        ++screenY;
        if (screenY > stageHeight)
        {
            down = false;
            up = true;
        }
    }
    if (left)
    {
        --screenX;
        if (screenX < -50) // very TMP
        {
            left = false;
            right = true;
        }
    }
    if (right)
    {
        ++screenX;
        if (screenX > stageWidth) // very TMP
        {
            right = false;
            left = true;
        }
    }

}




/*
 *  Draw enemy sprite, including the shade
 */
void Enemy::draw(QPainter *painter, int offsetX, int offsetY)
{
    // draw enemy's shade pixmap
    painter->drawPixmap(this->screenX + shadeOffsetX + offsetX,
                        this->screenY + shadeOffsetY + offsetY,
                        this->shadePixmap);

    // draw enemy pixmap
    painter->drawPixmap(this->screenX + offsetX,
                        this->screenY + offsetY,
                        this->spritePixmap1);

    // name
    painter->setPen(QColor("darkBlue"));
    painter->drawText(this->screenX + offsetX, this->screenY + offsetY,  this->name);
}
