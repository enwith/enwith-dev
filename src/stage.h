/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef STAGE_H
#define STAGE_H

#include <QObject>
#include <QPainter>
#include <QPixmap>
#include <QStringList>

#include <QDebug>


class Stage : public QObject
{
    Q_OBJECT

public:
    Stage(QObject *parent = 0);
    ~Stage();

    int getOffsetX();
    int getOffsetY();

signals:
//    void sizeChanged(int newWidth, int newHeight);


public slots:
    void load(int numStage);

    void move(int playerX, int playerY);

    void setSize(int windowWidth, int windowHeight);

    void drawBackground(QPainter *painter);
    void drawForeground(QPainter *painter);


private:
    int currentStage;
    QStringList backgroundFilenames;
    QStringList foregroundFilenames;


    int backgroundX;
    int backgroundY;

    int foregroundX;
    int foregroundY;

    int width;
    int height;

    int maxZ;


    QPixmap backgroundPixmap;
    QPixmap foregroundPixmap;

};

#endif // STAGE_H
