/*
 *   This file is part of Enwith
 *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "character.h"

class Player : public Character
{
    Q_OBJECT

public:
    Player(QObject *parent);
    ~Player();

signals:


public slots:
    void processKeyPresses(QKeyEvent *event);
    void processKeyReleases(QKeyEvent *event);


    void move();

    void draw(QPainter *painter);

private:

};



#endif // PLAYER_H
