# *
# *   This file is part of Enwith
# *   Copyright 2011-2015  JanKusanagi <jancoding@gmx.com>
# *
# *   This program is free software; you can redistribute it and/or modify
# *   it under the terms of the GNU General Public License as published by
# *   the Free Software Foundation; either version 2 of the License, or
# *   (at your option) any later version.
# *
# *   This program is distributed in the hope that it will be useful,
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *   GNU General Public License for more details.
# *
# *   You should have received a copy of the GNU General Public License
# *   along with this program; if not, write to the
# *   Free Software Foundation, Inc.,
# *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
#
# -------------------------------------------------
# Project created by QtCreator
# -------------------------------------------------
QT += opengl
     #multimedia
TARGET = enwith
TEMPLATE = app
SOURCES += src/main.cpp \
    src/mainwindow.cpp \
    src/player.cpp \
    src/stage.cpp \
    src/enemy.cpp \
    src/character.cpp
HEADERS += src/mainwindow.h \
    src/player.h \
    src/stage.h \
    src/enemy.h \
    src/character.h
OTHER_FILES += CHANGELOG \
    README \
    LICENSE
RESOURCES += \
    resources/stage.qrc \
    resources/player.qrc \
    resources/enemy.qrc \
    resources/general.qrc \
    resources/sound.qrc
TRANSLATIONS += translations/enwith_es_ES.ts \
                translations/enwith_ca_ES.ts
